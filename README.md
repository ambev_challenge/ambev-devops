# AMBEV DEVOPS:
O objetivo desse projeto é automatizar o processo de configurar o ambiente local para rodar a aplicação, executando os seguintes passos;
    1. `clone do repositório ambev-app, que contém as APIs`
    2. `Build do projeto via maven`
    3. `Criar imagem docker com o artefato gerado no processo de build`
    4. `Criar a imagem do mongo, migrando a estrutura de registro`
    5. `Subir os containers das aplicações`
    
Importante executar os comandos como descrito abaixo, devido ao pouco tempo, outros cenários não foram testados.

O README.md do projeto spring boot com as APIs está disponivel aqui (https://gitlab.com/ambev_challenge/ambev-pdv/blob/master/README.md)


# Local deste repositório
Em termos de padronização, é aconselhável que este repositório seja clonado em `~/challenge/brunofr/ambev-devops`

#### Clone
`git clone https://gitlab.com/ambev_challenge/ambev-devops.git ~/challenge/brunofr/ambev-devops`

# Instalação do ambiente local

Para instalar o ambiente local, será necessário instalar o ansible:

Há duas maneiras de instalar o ansible:
  - Via python utilizando `pip`
    > pip é um sistema gerenciador de pacotes usado para instalar e gerenciar softwares escritos em python.

  - Via apt-get instalando diretamento o ansible via apt-get

 **Via pip** <br />
    É recomendado instalar o ansible via pip, devido a facilidade de atualizar a versão do ansible. Para realizar a instalação será necessário executar esses comandos:

    1. `sudo apt-get install python-pip python-dev`
    2. `sudo pip install --upgrade pip`
    3. `sudo pip install ansible`

 **Via apt-get** <br />
    Executar a seguinte sequência de comandos:
    
    1. Install the software-properties-common package
    `sudo apt-get update && sudo apt-get install software-properties-common`

    2. Add the Ansible repository to your system
    `sudo apt-add-repository ppa:ansible/ansible`

    3. Install Ansible
    `sudo apt-get update && sudo apt-get install ansible`

Após instalar o ansible, execute o seguinte comando em ~/challenge/brunofr/ambev-devops/ansible <br>
`$ ansible-playbook main.yml -K`

# Ambiente de teste local

`curl -X GET \
  http://localhost:8080/api/seller/6 \
  -H 'cache-control: no-cache'`

